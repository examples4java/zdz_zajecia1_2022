import java.io.IOException;
import java.util.Scanner;

public class Main {
	
	static void dodaj(int a) {
		dodaj(a,0);
	}
	
	static void dodaj(int a, int b) {
		dodaj((double)a, (double)b);
	}
	
	static void dodaj(double a, double b) {
		System.out.println(a + " + " + b + " = " + (a+b));	
	}
	
	static int silnia(long n) {
		int w=1;
		if (n<0) //je�eli liczba mniejsza ni� zero to silni nie ma - ustawiamy warto�� -1
			return -1;
		if (n==0) //dla zera silnia zawsze wynosi 1
			return 1;
		while(n>0) {
			w*=n--;
		}
		return w;
	}
	
	static long silniaR(long n) {
		/*
		 * Poni�ej przyk�ad operatora tr�jstanowego. Pozwalna on na zapis warunku i 
		 * szybkiej reakcji warunku na pozytwny wynik wyra�enia logicznego, jak i na wynik
		 * negatywny (operator ten ZAWSZE przyjmuje uk�ad if...else)
		 * 
		 * (warunek) ? <instrukcja_gdy_prawda> : <instrukcja_gdy_fa�sz>
		 * 
		 * warunek - dowolny warunek logiczny, mo�e by� ��czony porzez && oraz ||
		 * instrukcja_gdy_prawda - pojeydczy rozkaz, kt�ry wykona si�, gdy wymieniony wczesniej
		 * warunek b�dzie prawdziwy (np. 7<10)
		 * instrukcja_gdy_fa�sz - pojedynczy rozkaz, kt�ry wykona si�, gdy wymieniony wcze�niej
		 * warunke b�dzie fa�szywy (np. 7<3).
		 * 
		 * Instrukcja zast�puje nast�puj�c� konstrukcj� if..else
		 * 
		 * if(a<b)
		 * 		<instrukcja_gdy_prawda>
		 * else 
		 * 		<instrukcja_gdy_fa�sz>
		 * 
		 * Zalet� operatora tr�jstanowego jest to, �e mo�na jego wynik pdostawi� jako podinstrukcj�
		 * innej, wi�kszej instrukcji (rozkazu). Przyk�adowo zamiast pisa�:
		 * 
		 * if (n<=0) 
		 * 	return 1
		 * else
		 *  return 2
		 *  
		 * mo�na zapisa� 
		 * 
		 * return (n<=0) ? 1 : 2;
		 */
		return (n<=0) ? 1 : n*silniaR(n-1);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//podstawowe jednostki zmiennych w Java
		//boolean - test okre�lonego warunku daj�cy w odpowiedzi albo prawd�, albo fa�sz
		//1 bajt
		boolean test = true;
		//liczba ca�kowita pozwalaj�ca zapisa� warto�ci do ok 2 miliard�w.
		//4 bajty
		int liczba=56;
		//po�owa liczby int. 
		//2 bajty
		short liczba2 = -15;
		//podwojona liczba int
		//8 bajty
		long liczb3=98331;
		//zmiennoprzecinkowe pojedynczej precyzji
		//4 bajty
		float zm1 = 9.56f;
		//zmiennoprzecinkowe podw�jnej precyzji
		//8 bajt�w
		double zm2 = -11.31;
		//pojedynczy znak
		//1 bajt
		char znak = 'p';
		//zmienna bajtowa (dowolne dane mieszcz�ce si� w granicy jednego bajta - na max 8 bitach)
		//1 bajt
		byte bajt = 0x45;
		//BRAK wskaznik�w  w j�zyku Java!
		//char  *aaa="ajh"; //tutaj b�dzie b��d!
		//zapis ci�gu znakowego w j�zyku Java.
		String ciag = "Nowa warto�� tekstowa";
		ciag+=" nowa wiadomo��";
		//sta�e w Java rozpoczyna si� od s�owa kluczowego final; pozwala to na utworzenie
		//"zmiennej", kt�rej warto�� nie mo�e zosta� zmieniona przez ca�y okres jej �ycia
		final double stala = 89;
		
		System.out.println(silniaR(5));
		System.out.println(silniaR(6));
		
		
		
		/*
		 * Latwiejsze rozwi�zanie wczytywania danych od u�ytkownika z konsoli
		 * to wykorzystanie klasy Scanner, kt�ra pozwala na zautomatyzowanie 
		 * przyechytywania danych z konsoli i automatycznego zamieniania ich typu
		 * na warto�ci wskazane przez zmienne.
		 * 
		 * Domy�lnie Scanner zamienia warto�ci na String, ale mo�na skorzysta� z int (nextInt()),
		 * double (nextDouble()) i innych.
		 */
		System.out.print("Jaki jest Tw�j ulubiony dzie�?");
		Scanner scan = new Scanner(System.in);
		String dzien = scan.next();
		System.out.println("Dzie� to " + dzien);
		
		
		/* Domy�lnie Java nie posiada prostego rozwi�zania dotycz�ce wczytywania danych
		 * od u�ytkownika do programu (do zmiennych). 
		 * Standardowo korzysta ona z czytania pojedynczych znak�w, interperowanych jako bajty.
		 * Domy�lne metody pozwalaj� na wczytywanie zar�wno jednego, jak i okre�lonej ilo�ci bajt�w
		 * do tablicy bajtowej.
		 * 
		 * Celem wczytania jedynie po��danej ilo�ci bajt�w trzeba wykonac odpowiednie intrukcje
		 * p�tli oraz blok�w warunkowych, co niekiedy mo�e by� problematyczne
		 */
//		byte[] zapis = new byte[255];		
//		int val=0;
//		int i=0;
//		try {
//			while((val = System.in.read())!=13) {
//				zapis[i++]=(byte)val;
//			}
//			//zapis=System.in.readNBytes(10);
//			for (int j=0;j<i;j++) {
//				System.out.print(zapis[j]);
//				System.out.println(", " + (char)zapis[j]);
//			}
//			//System.out.println(new String(zapis));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
	}

}
